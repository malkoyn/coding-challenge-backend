from marshmallow import fields, Schema
from marshmallow.validate import OneOf, Range


class RepositoryItemSchema(Schema):
    full_name = fields.String()
    html_url = fields.Url()
    description = fields.String()
    stargazers_count = fields.Integer()
    language = fields.String()
    pushed_at = fields.String()


class RepositorySearchSchema(Schema):
    order = fields.Integer(validate=OneOf((-1, 1)))
    sort_by = fields.String(validate=OneOf(RepositoryItemSchema().fields))
    cursor = fields.Integer(validate=Range(min=0))
    limit = fields.Integer(validate=Range(min=1, max=100))
