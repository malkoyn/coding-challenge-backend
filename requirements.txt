aiohttp==3.1.3
marshmallow==2.9.1
pytest-aiohttp==0.3.0
https://github.com/ilex/aiomotorengine/archive/master.zip # aiomotorengine - async work with mongoDb
