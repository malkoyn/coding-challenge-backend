class LimitExceededException(Exception):
    """GitHub limit exceeded exception"""


class InvalidSearchSection(Exception):
    """Search section is invalid"""
