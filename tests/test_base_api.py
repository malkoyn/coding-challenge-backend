import pytest

from app import setup_app


@pytest.fixture(scope='function')
def cli(loop, aiohttp_client):
    app = setup_app()
    return loop.run_until_complete(aiohttp_client(app))


async def test_getting_repositories_success(cli):
    resp = await cli.get('/repositories?limit=2')
    assert resp.status == 200

    data = await resp.json()
    assert data.get('result') is not None

    # only 2 should be returned
    assert len(data['result']) == 2

# TODO: more tests.
