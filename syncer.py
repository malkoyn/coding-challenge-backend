import abc
import asyncio
import datetime
import logging

import processors
import models

from git_client.git_client import GitClient
from exceptions import LimitExceededException

logger = logging.getLogger()


class Syncer(abc.ABC):
    """Syncer abstract class"""

    @property
    @abc.abstractmethod
    def processor(self) -> processors.ProcessorABC:
        pass

    @property
    @abc.abstractmethod
    def model(self) -> models.Document:
        pass

    @abc.abstractmethod
    def sync(self) -> int:
        pass


class RepositoriesSyncer(Syncer):
    """GitHub repositories syncer"""
    processor = processors.RepositoryProcessor
    model = models.RepositoryModel

    def __init__(self, min_stars=500, language='python'):
        """
        :param min_stars: minimum stars in repositories. All repositories which have less stars will be skipped.
        """
        self.language = language
        self.min_stars = min_stars
        self._batch = 100
        self._batch_timeout = 60  # seconds

    async def sync(self) -> int:
        """
        Sync the repositories from GitHub.
        This function will pause after 10th(30) requests for one minute cuz of GitHub restrictions.
        See more info: https://developer.github.com/v3/search/
        :return: count of synced repositories
        """
        logger.info('sync started.')

        updated_cursor = datetime.datetime.min.replace(year=1970).isoformat()  # min iso value
        query_tmpl = 'language:%(language)s stars:>%(min_stars)d pushed:>%(updated)s'
        total_repos = 0
        while True:
            query = query_tmpl % {'min_stars': self.min_stars, 'updated': updated_cursor, 'language': self.language}
            try:
                response = await GitClient().search(
                    'repositories', query=query, sort='updated', order='asc', per_page=self._batch, page=1
                )
            except LimitExceededException:
                # from one IP github API accept 1000 requests per minute, we will split requests by batches.
                # set timeout between requests
                logger.info("sync paused. Waiting one minute...")
                await asyncio.sleep(self._batch_timeout)
                logger.info("sync resumed...")

            else:
                data = self.processor.process(response)
                updated_cursor = data[-1]['pushed_at']
                # save data
                for repo_data in data:
                    await self.model(**repo_data).save()  # TODO: need to upsert data.
                total_repos += len(data)

                if len(data) < self._batch:
                    logger.info('Sync count: %d' % total_repos)
                    logger.info('sync finished.')
                    return total_repos
