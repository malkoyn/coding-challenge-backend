import asyncio
import aiomotorengine
import handlers
import routes

from aiohttp import web


def setup_app():
    app = web.Application()
    routes.setup(app, handlers)

    aiomotorengine.connection.connect('gitsi', host="localhost", io_loop=asyncio.get_event_loop())

    return app


if __name__ == "__main__":
    app = setup_app()
    web.run_app(app, host='0.0.0.0', port=8000)
