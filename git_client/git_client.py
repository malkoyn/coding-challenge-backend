import aiohttp
import ssl

from exceptions import LimitExceededException, InvalidSearchSection


class GitClient:
    """
        Client for working with github API.
    """
    BASE_URL_SCHEMA = 'https'
    BASE_URL = '://'.join((BASE_URL_SCHEMA, 'api.github.com'))

    SECTIONS = (
        'repositories',
        'commits',
        'code',
        'issues'
    )

    @staticmethod
    def _construct_path(*args):
        return '/'.join(args)

    async def search(
            self, section: str, query: str, sort: str ='',
            order: str ='desc', per_page: int =100, page: int =1) -> list:
        """
        Search github by section.
        For more info visit https://developer.github.com/v3/search/
        :param section: github section. Ex.: repositories, commits, code, issues etc.
        :param query: query for the search
        :param sort: parameter for sorting, default is by best match
        :param order: ascending or descending order, default is by descending
        :param per_page: items per page
        :param page: page number
        :return: list of searched items
        """
        if section not in self.SECTIONS:
            raise InvalidSearchSection

        async with aiohttp.ClientSession() as session:
            async with session.get(
                    self._construct_path(self.BASE_URL, 'search', section),
                    params={'q': query, 'sort': sort, 'order': order, 'per_page': per_page, 'page': page},
                    ssl=ssl.SSLContext()) as response:
                if response.status == 403:
                    raise LimitExceededException
                return await response.json()

