import abc

from serializers import RepositoryItemSchema


class ProcessorABC(abc.ABC):
    @abc.abstractmethod
    def process(self, data: dict) -> list:
        pass


class RepositoryProcessor:
    @staticmethod
    def process(data):
        data = data.get('items', [])
        schema = RepositoryItemSchema().load(data, many=True)
        return schema.data
