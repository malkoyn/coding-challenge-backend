import syncer

from models import RepositoryModel
from serializers import RepositorySearchSchema

from aiohttp import web


async def sync_repositories(request):
    """Handler for syncing repositories"""
    # TODO: should be runned in separate thread.
    repos_count = await syncer.RepositoriesSyncer().sync()
    return web.json_response({'result': {'sync_count': repos_count}})


async def repositories(request):
    """Handler for getting repositories"""
    q_params = request.rel_url.query

    schema = RepositorySearchSchema().load(q_params)
    if schema.errors:
        return web.json_response({'result': schema.errors}, status=400)

    data = schema.data
    repos = await RepositoryModel.get_repositories(**data)
    return web.json_response({
        'result': [repo.to_son() for repo in repos],
        'cursor': data.get('cursor', 0) + len(repos)
    })
