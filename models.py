from aiomotorengine import Document
from aiomotorengine import StringField, URLField, IntField


class RepositoryModel(Document):
    __collection__ = 'repositories'

    full_name = StringField(required=True)
    html_url = URLField(required=True)
    description = StringField()
    stargazers_count = IntField(required=True)
    language = StringField()
    pushed_at = StringField()

    @classmethod
    def get_repositories(cls, cursor=0, limit=100, sort_by='stargazers_count', order=-1):
        return cls.objects.order_by(sort_by, direction=order).skip(cursor).limit(limit).find_all()
