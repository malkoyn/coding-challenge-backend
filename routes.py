def setup(app, handlers):
    url = app.router.add_route
    url('GET', '/sync/repositories', handlers.sync_repositories, name='sync_repos')
    url('GET', '/repositories', handlers.repositories, name='get_repos')
